import sys

data = []
current = []

for line in sys.stdin:
    if not "-=-" in line:
        current.append(line.strip())
    else:
        data.append(current)
        current = []
total = len(data)
train = int(0.6 * total)
test = int(total - train)

print total, train, test

with open("train.data", "a") as train_f:
    for record in data[0:train]:
        train_f.write("\n".join(record) + "\n")
        train_f.write("\n")
    train_f.close()

with open("test.data", "a") as test_f:
    for record in data[train:total]:
        test_f.write("\n".join(record) + "\n")
        test_f.write("\n")
    test_f.close()
