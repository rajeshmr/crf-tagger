__author__ = 'raj'
from flask import Flask, request, jsonify, abort

import nltk
import logging, sys
from crfutils.chunking import feature_extractor
from crfutils.crfutils import to_crfsuite
import crfsuite
import sys
from collections import defaultdict

app = Flask(__name__)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
app.logger.addHandler(ch)
tagger = crfsuite.Tagger()
tagger.open(sys.argv[1])
print "initialized model"


@app.route("/")
def hello():
    return "Hello World!"


def pre_feature(tagged_tokens):
    for_feature = []
    for record in tagged_tokens:
        token_features = []
        for token in record:
            feature = {"y": "XXX", "w": token[0], "pos": token[1], "F": []}
            token_features.append(feature)
        for_feature.append(token_features)
    return for_feature


@app.route("/tag", methods=['GET', 'POST'])
def tag():
    content = request.get_json(silent=True)
    if len(content) > 50:
        return abort(400)
    content = map(lambda x: x.lower(), content)
    tokens = map(nltk.word_tokenize, content)
    tagged_tokens = map(nltk.pos_tag, tokens)
    for_feature = pre_feature(tagged_tokens)
    with_feature = map(feature_extractor, for_feature)
    flattened_with_feature = [item for sublist in with_feature for item in sublist]
    xseq = to_crfsuite(flattened_with_feature)
    yseq = tagger.tag(xseq)
    tags = []
    for y in yseq:
        tags.append(y)
    tags = list(reversed(tags))
    result = []
    for feature in with_feature:
        tagged_token = defaultdict(list)
        for token in feature:
            tag = tags.pop()
            if token['w'] not in tagged_token[tag]:
                tagged_token[tag].append(token['w'])
        result.append(tagged_token)
    return jsonify(tagged_tokens=result)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
