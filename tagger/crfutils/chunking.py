import crfutils
import copy

templates = (
    (('w', -2),),
    (('w', -1),),
    (('w', 0),),
    (('w', 1),),
    (('w', 2),),
    (('w', -1), ('w', 0)),
    (('w', 0), ('w', 1)),
    (('pos', -2),),
    (('pos', -1),),
    (('pos', 0),),
    (('pos', 1),),
    (('pos', 2),),
    (('pos', -2), ('pos', -1)),
    (('pos', -1), ('pos', 0)),
    (('pos', 0), ('pos', 1)),
    (('pos', 1), ('pos', 2)),
    (('pos', -2), ('pos', -1), ('pos', 0)),
    (('pos', -1), ('pos', 0), ('pos', 1)),
    (('pos', 0), ('pos', 1), ('pos', 2)),
)


def feature_extractor(pos_input):
    output = copy.deepcopy(pos_input)
    crfutils.apply_templates(output, templates)
    if output:
        output[0]['F'].append('__BOS__')
        output[-1]['F'].append('__EOS__')
    return output
