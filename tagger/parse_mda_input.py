import json
import sys
import nltk

print "\t".join(["token", "pos", "tag"])
for line in sys.stdin:
    data = json.loads(line)
    if "longDescription" in data:
        try:
            desc = data["longDescription"]
            desc = desc.lower().strip()
            tokens = nltk.word_tokenize(desc)
            pos_tokens = nltk.pos_tag(tokens)
            for i, (token, pos) in enumerate(pos_tokens):
                print "\t".join([token, pos, "XXX"]).encode('utf-8')
            print "-=-"
        except Exception as e:
            print e
            break
